# pcg_bfloat_comparison

Octave/Matlab program to compare data types to see if the floating level will change the interaction counts when running a precondition conjugate gradient algorithm. 

## Requires
This program requires :
* Chop : https://github.com/higham/chop
* MMRead : https://www.mathworks.com/matlabcentral/fileexchange/8028-mmread
